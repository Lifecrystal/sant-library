# Sant-library
[![Maven Central](https://img.shields.io/maven-central/v/cloud.testload/sant-library.svg?label=Maven%20Central)](https://search.maven.org/search?q=g:%22cloud.testload%22%20AND%20a:%22sant-library%22)
[![Javadocs](https://www.javadoc.io/badge/cloud.testload/sant-library.svg)](https://www.javadoc.io/doc/cloud.testload/sant-library)
![pipeline](https://gitlab.com/testload/sant-library/badges/master/build.svg?job=build)

jar for automated loadtest

Many thanks for support from:

[JetBrains](https://www.jetbrains.com/opensource/)

[Atlassian](https://www.atlassian.com/software/views/open-source-license-request)

