<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>cloud.testload</groupId>
    <artifactId>sant-library</artifactId>
    <version>1.9.82-SNAPSHOT</version>
    <packaging>jar</packaging>
    <name>SANTLibrary</name>

    <description>Groovy library for automation of loadtesting</description>
    <url>https://gitlab.com/testload/sant-library</url>

    <licenses>
        <license>
            <name>The MIT License</name>
            <url>https://opensource.org/licenses/MIT</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <scm>
        <url>https://gitlab.com/testload/sant-library.git</url>
    </scm>

    <developers>
        <developer>
            <id>lansio</id>
            <name>Alexander Babaev</name>
            <email>testload@icloud.com</email>
            <url>https://gitlab.com/testload</url>
            <timezone>Europe/Moscow</timezone>
        </developer>
    </developers>

    <repositories>
        <repository>
            <id>springM</id>
            <name>SpringM</name>
            <url>https://repo.spring.io/libs-milestone/</url>
        </repository>
    </repositories>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <java.version>1.8</java.version>
        <groovy.version>2.4.15</groovy.version>
        <org.influxdb.influxdb-java>2.13</org.influxdb.influxdb-java>
        <org.jsoup.jsoup>1.6.2</org.jsoup.jsoup>
        <de.gesellix.docker-client>2018-06-02T10-49-31</de.gesellix.docker-client>
        <org.codehaus.groovy.modules.http-builder>0.7.1</org.codehaus.groovy.modules.http-builder>
        <com.google.code.gson>2.8.5</com.google.code.gson>
        <com.squareup.okhttp3>3.11.0</com.squareup.okhttp3>
        <flexmark.version>0.34.56</flexmark.version>
        <kuberclient.version>4.2.2</kuberclient.version>
        <ru.yandex.clickhouse.clickhouse-jdbc>0.1.53</ru.yandex.clickhouse.clickhouse-jdbc>
        <groovy-sql-version>2.0.1</groovy-sql-version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>io.fabric8</groupId>
            <artifactId>kubernetes-client</artifactId>
            <version>${kuberclient.version}</version>
        </dependency>
        <dependency>
            <groupId>com.vladsch.flexmark</groupId>
            <artifactId>flexmark-all</artifactId>
            <version>${flexmark.version}</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
            <version>${groovy.version}</version>
            <type>pom</type>
        </dependency>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-xml</artifactId>
            <version>${groovy.version}</version>
        </dependency>
        <dependency>
            <groupId>org.influxdb</groupId>
            <artifactId>influxdb-java</artifactId>
            <version>${org.influxdb.influxdb-java}</version>
        </dependency>
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>${org.jsoup.jsoup}</version>
        </dependency>
        <dependency>
            <groupId>de.gesellix</groupId>
            <artifactId>docker-client</artifactId>
            <version>${de.gesellix.docker-client}</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.groovy.modules.http-builder</groupId>
            <artifactId>http-builder</artifactId>
            <version>${org.codehaus.groovy.modules.http-builder}</version>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>${com.google.code.gson}</version>
        </dependency>
        <dependency>
            <groupId>com.squareup.okhttp3</groupId>
            <artifactId>okhttp</artifactId>
            <version>${com.squareup.okhttp3}</version>
        </dependency>
        <dependency>
            <groupId>com.squareup.okhttp3</groupId>
            <artifactId>okhttp-urlconnection</artifactId>
            <version>${com.squareup.okhttp3}</version>
        </dependency>
        <dependency>
            <groupId>ru.yandex.clickhouse</groupId>
            <artifactId>clickhouse-jdbc</artifactId>
            <version>${ru.yandex.clickhouse.clickhouse-jdbc}</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-sql</artifactId>
            <version>${groovy-sql-version}</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.spockframework</groupId>
            <artifactId>spock-core</artifactId>
            <version>1.3-groovy-2.4</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>1.5</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.18.1</version>
                <configuration>
                    <includes>
                        <include>**/*Test.java</include>
                        <include>**/*Spec.java</include>
                    </includes>
                    <environmentVariables>
                        <SOME_ENV_VARIABLE>TEST1</SOME_ENV_VARIABLE>
                    </environmentVariables>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.sonatype.plugins</groupId>
                <artifactId>nexus-staging-maven-plugin</artifactId>
                <version>1.6.7</version>
                <extensions>true</extensions>
                <configuration>
                    <serverId>ossrh</serverId>
                    <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                    <autoReleaseAfterClose>true</autoReleaseAfterClose>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>2.2.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>com.bluetrainsoftware.maven</groupId>
                <artifactId>groovydoc-maven-plugin</artifactId>
                <version>2.1</version>
                <executions>
                    <execution>
                        <id>attach-docs</id>
                        <phase>package</phase>
                        <goals>
                            <goal>attach-docs</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.1</version>
                <executions>
                    <execution>
                        <id>default-deploy</id>
                        <phase>deploy</phase>
                        <goals>
                            <goal>deploy</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.gmavenplus</groupId>
                <artifactId>gmavenplus-plugin</artifactId>
                <version>1.6</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>addSources</goal>
                            <goal>addTestSources</goal>
                            <goal>generateStubs</goal>
                            <goal>compileTests</goal>
                            <goal>compile</goal>
                            <goal>groovydoc</goal>
                            <goal>removeStubs</goal>
                            <goal>removeTestStubs</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>